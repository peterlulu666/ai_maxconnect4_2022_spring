Name: Yanzhi Wang
UTA ID: 1001827416
Programming language: Python 3
It is not omega compatible
How the code is structured
1. In the provided code, the aiPlay is playing randomly
2. We are implementing Minimax in maxconnect4, so the ai is able to make the best move
3. Minimax algorithm is a recursive algorithm. We have Max and Min player.
Max will try to maximize the value. Min will try to minimize the value.
The algorithm performs a depth-first search (DFS) which means
it will explore the complete game tree as deep as possible,
all the way down to the leaf nodes.
4. In particular,
Initially, the algorithm generates the entire game tree and
produces the utility values for the terminal states by
applying the utility function.
In the utility function score_position,
I count ai piece or computer piece for each column and get the max.
Then, we compare the values from each node with the value of the Maximizer, which is -∞.
Next, we compare the values from each node with the value of the minimizer, which is +∞.
Finally, the maximizer will select the maximum value.
5. Thereafter,
I extend the original Minimax algorithm by adding the
Alpha-beta pruning strategy to improve
the computational speed and save memory.
6. In the provided code, the interactive mode is not available.
I completed the interactive mode.
We check if the game consists of 42 moves,
if it is not consists of 42 moves,
the player and the computer can play one after another.
If it is player's turn, player can input piece,
we check if the input is valid.
In particular, we check if the input column is between 1 and 7.
We check if all positions in the same column are occupied.
We save the current board state in a file called human.txt.
We print result and switch turn.
If it is computer's turn, the code is the same as the one_move.
7. There is known bug for computer-next in the Interactive Mode.
8. There is known bug for some input file in the One-Move Mode.
How to run the code
1. If you are running One-Move Mode
./maxconnect4.py one-move input1.txt output1.txt 3
2. If you are running Interactive Mode
./maxconnect4.py interactive input1.txt human-next 3
