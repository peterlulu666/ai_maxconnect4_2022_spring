#!/usr/bin/env python

# Written by Chris Conly based on C++
# code provided by Dr. Vassilis Athitsos
# Written to be Python 2.4 compatible for omega

import sys
from MaxConnect4Game import *


def oneMoveGame(currentGame, set_depth):
    if currentGame.pieceCount == 42:  # Is the board full already?
        print('BOARD FULL\n\nGame Over!\n')
        sys.exit(0)

    # currentGame.aiPlay()  # Make a move (only random is implemented)

    # AI not random
    currentGame.aiPlay(currentGame, set_depth)

    print('Game state after move:')
    currentGame.printGameBoard()

    currentGame.countScore()
    print('Score: Player 1 = %d, Player 2 = %d\n' % (currentGame.player1Score, currentGame.player2Score))

    currentGame.printGameBoardToFile()
    currentGame.gameFile.close()


def interactiveGame(currentGame, set_depth):
    # Fill me in

    # player piece is 1
    PLAYER_PIECE = 1
    # ai piece is 2
    AI_PIECE = 2

    while not currentGame.pieceCount == 42:
        # player turn
        if currentGame.currentTurn == PLAYER_PIECE:
            # We check if the input column number is between 1 and 7
            # We check if the same column is full
            playerColumn = int(input("You select the position: "))
            if not 0 < playerColumn < 8:
                print("Columns are numbered from left to right, with numbers 1, 2, ..., 7")
                continue
            if not currentGame.playPiece(playerColumn - 1):
                print("All positions in the same column are occupied")
                continue
            # Save the current board state in a file called human.txt
            try:
                currentGame.gameFile = open("human.txt", 'w')
            except:
                sys.exit('Error opening output file.')
            # print result
            print(
                '\n\nmove %d: Player %d, column %d\n' % (currentGame.pieceCount, currentGame.currentTurn, playerColumn))
            # switch turn
            if currentGame.currentTurn == 1:
                currentGame.currentTurn = 2
            elif currentGame.currentTurn == 2:
                currentGame.currentTurn = 1

            print('Game state after move:')
            currentGame.printGameBoard()

            currentGame.countScore()
            print('Score: Player 1 = %d, Player 2 = %d\n' % (currentGame.player1Score, currentGame.player2Score))

            currentGame.printGameBoardToFile()

        # ai turn
        elif currentGame.currentTurn == AI_PIECE:
            # It is the same as the one_move
            # AI not random
            currentGame.aiPlay(currentGame, set_depth)

            print('Game state after move:')
            currentGame.printGameBoard()

            currentGame.countScore()
            print('Score: Player 1 = %d, Player 2 = %d\n' % (currentGame.player1Score, currentGame.player2Score))

            currentGame.printGameBoardToFile()

    # sys.exit('Interactive mode is currently not implemented')


def main(argv):
    # Make sure we have enough command-line arguments
    if len(argv) != 5:
        print('Four command-line arguments are needed:')
        print('Usage: %s interactive [input_file] [computer-next/human-next] [depth]' % argv[0])
        print('or: %s one-move [input_file] [output_file] [depth]' % argv[0])
        sys.exit(2)

    game_mode, inFile = argv[1:3]

    if not game_mode == 'interactive' and not game_mode == 'one-move':
        print('%s is an unrecognized game mode' % game_mode)
        sys.exit(2)

    currentGame = maxConnect4Game()  # Create a game

    # Try to open the input file
    try:
        currentGame.gameFile = open(inFile, 'r')
    except IOError:
        sys.exit("\nError opening input file.\nCheck file name.\n")

    # Read the initial game state from the file and save in a 2D list
    file_lines = currentGame.gameFile.readlines()
    currentGame.gameBoard = [[int(char) for char in line[0:7]] for line in file_lines[0:-1]]
    currentGame.currentTurn = int(file_lines[-1][0])
    currentGame.gameFile.close()

    print('\nMaxConnect-4 game\n')
    print('Game state before move:')
    currentGame.printGameBoard()

    # Update a few game variables based on initial state and print the score
    currentGame.checkPieceCount()
    currentGame.countScore()
    print('Score: Player 1 = %d, Player 2 = %d\n' % (currentGame.player1Score, currentGame.player2Score))

    if game_mode == 'interactive':
        # player piece is 1
        PLAYER_PIECE = 1
        # ai piece is 2
        AI_PIECE = 2
        if argv[3] == 'computer-next':  # override current turn according to commandline arguments
            currentGame.currentTurn = AI_PIECE
        elif argv[3] == 'human-next':
            currentGame.currentTurn = PLAYER_PIECE
        interactiveGame(currentGame, argv[4])  # Be sure to pass whatever else you need from the command line
    else:  # game_mode == 'one-move'
        # Set up the output file
        outFile = argv[3]
        try:
            currentGame.gameFile = open(outFile, 'w')
        except:
            sys.exit('Error opening output file.')
        oneMoveGame(currentGame, argv[4])  # Be sure to pass any other arguments from the command line you might need.


if __name__ == '__main__':
    main(sys.argv)
